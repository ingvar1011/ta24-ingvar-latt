
const getCountOfLetters = (sentence) => {
    const containerObject = {};
    for (let i = 0; i < sentence.length; i++) {
        // checks if current character is in the alphabet
        if (!/[a-zA-Z]/.test(sentence.charAt(i))) {
            continue;
        }
        // if it doesnt exist, creates a property for the current
        // character in containerObject
        if (!containerObject[sentence.charAt(i)]) {
            containerObject[sentence.charAt(i)] = 0;
        }
        containerObject[sentence.charAt(i)]++;
    }
    return containerObject;
}

const result = getCountOfLetters("a black cat");
console.log(result);
