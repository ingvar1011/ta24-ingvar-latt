const calculateTotalScore = (scoreLine) => {
    const lookUp = {
    S: 8,
    A: 6,
    B: 4,
    C: 3,
    D: 2,
    F: 0
    }
    let total = 0;
    for(let i = 0; i < scoreLine.length; i++){
        total += lookUp[scoreLine.charAt(i)];
    }
    return total;
}

const caluclateAverageScore = (scoreLine) => {return calculateTotalScore(scoreLine)/(scoreLine.length);}

const totalScore = calculateTotalScore("DFCBDABSB");
console.log(totalScore); // prints 33
const avgScore = caluclateAverageScore("DFCBDABSB");
console.log(avgScore);
console.log([ "AABAACAA", "FFDFDCCDCB", "ACBSABA", "CCDFABABC" ].map(scores => caluclateAverageScore(scores)));