class Animal {
    constructor(weight, cuteness){
        this.weight = weight,
        this.cuteness = cuteness
    }
    makeSound(){
        console.log("silence");
    }
}
class Cat extends Animal {
    constructor(weight, cuteness){
        super(weight, cuteness)
    }
    makeSound(){
        console.log("meow");
    }
}
class Dog extends Animal {
    constructor(weight, cuteness, breed){
        super(weight, cuteness),
        this.breed = breed
    }
    makeSound() {
        if(this.cuteness > 4){
            console.log("awoo");
        }else console.log("bark");
    }
}

const animal = new Animal(6.5, 4.0);
animal.makeSound();
console.log(animal);

const cat = new Cat(4.5, 3.0);
cat.makeSound();
console.log(cat);

const dog1 = new Dog(7.0, 4.5, "kleinspitz");
const dog2 = new Dog(30.0, 3.75, "labrador");
dog1.makeSound();
console.log(dog1);
dog2.makeSound();
console.log(dog2);
