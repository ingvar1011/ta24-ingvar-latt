class WeatherEvent {
    constructor(timestamp){
        this.timestamp = timestamp;
    }
    getInformation(){
        return "";
    }
    print(){
        console.log(`${this.timestamp} ${this.getInformation()}`)
    }
}

class TemperatureChangeEvent extends WeatherEvent {
    constructor(timestamp, temperature){
        super(timestamp);
        this.temperature = temperature;
    }
    getInformation(){
        return `temperature: ${this.temperature}°C`;
    }
}

class HumidityChangeEvent extends WeatherEvent {
    constructor(timestamp, humidity){
        super(timestamp);
        this.humidity = humidity;
    }
    getInformation(){
        return `humidity: ${this.humidity}%`;
    }
}

class WindChangeEvent extends WeatherEvent {
    constructor(timestamp, wind){
        super(timestamp);
        this.wind = wind;
    }
    getInformation(){
        return `wind: ${this.wind} m/s`
    }
}

const weatherEvents = [];

const date1 = new Date('March 25, 2024 13:58');
const date2 = new Date('March 25, 2024 14:00');
const date3 = new Date('March 25, 2024 14:06');


weatherEvents.push(new TemperatureChangeEvent(date1, -2.6));
weatherEvents.push(new HumidityChangeEvent(date2, 73));
weatherEvents.push(new WindChangeEvent(date3, 1));

weatherEvents.forEach(weatherEvent => weatherEvent.print());

