class Room  {
    constructor(width, height){
        this.width = width,
        this.height = height,
        this.furniture = []
    }
    getArea() {
        return this.width * this.height;
    }
    addFurniture(item) {
        this.furniture.push(item);
    }
}

const room = new Room(4.5, 6.0);
const area = room.getArea();
room.addFurniture("sofa");
room.addFurniture("bed");
room.addFurniture("chair");
console.log(room);