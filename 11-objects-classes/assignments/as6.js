class Robot {
    constructor(x, y){
        this.x = x,
        this.y = y
    }
    handleCommandList(commandList) {
        const commandHandlers = {
            N: () => this.y++,
            S: () => this.y--,
            E: () => this.x++,
            W: () => this.x--,
            C: () => {}
        }

        for (let i = 0; i < commandList.length; i++){
            if(commandList.charAt(i) === "B"){
                break;
            }
            else commandHandlers[commandList.charAt(i)]();
        }
    }

}

const robot = new Robot(0, 0);
const robot2 = new Robot(2, 3);
console.log(robot);
console.log(robot2);
robot.handleCommandList("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
robot2.handleCommandList("NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENNNEEE");
console.log(robot);
console.log(robot2);