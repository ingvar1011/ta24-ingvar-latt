const commandList = "NNEESSWWCNNEEENNNCEESSSWNNNECEESWWNNNEEEBENBNEEE";
const commandHandlers = {
    N: () => y++,
    S: () => y--,
    E: () => x++,
    W: () => x--,
    C: () => {}
}
let x = 0;
let y = 0;

for (let i = 0; i < commandList.length; i++){
    if(commandList.charAt(i) === "B"){
        break;
    }
    else commandHandlers[commandList.charAt(i)]();
}

console.log(`x is ${x}, y is ${y}.`);