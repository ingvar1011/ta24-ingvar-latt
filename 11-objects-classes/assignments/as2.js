const translations = {
    hello: "hei",
    world: "maailma",
    bit: "bitti",
    byte: "tavu",
    integer: "kokonaisluku",
    boolean: "totuusarvo",
    string: "merkkijono",
    network: "verkko"
}

const printTranslateWords = () => {
    console.log(Object.keys(translations));
}
const translate = (word) => {
    if(translations[word]){
        return translations[word];
    }else{
        console.log(`No translation exists for "${word}"`);
        return null;
    }
}
printTranslateWords();
console.log(translate("strg"))