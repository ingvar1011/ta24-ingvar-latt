const fruits = {
    banana: 118,
    apple: 85,
    mango: 200,
    lemon: 65
};
const printWeight = function (fruit) {
    if (fruit in fruits) {
        console.log(fruit + " weighs " + fruits[fruit] + "grams");
    } else {
        console.log("There is no such fruit, only fruits are: " +
            Object.keys(fruits).join(", "));
    }
};
printWeight("banana");
printWeight("rat");


// exercise 2

const student = {
    name: "Aili",
    credits: 45,
    courseGrades: {
        "Intro to Programming": 4,
        "JavaScript Basics": 3,
        "Functional Programming": 5
    }
}
console.log(student);

student.courseGrades["Program Design"] = 3;
console.log(student);

student.courseGrades["JavaScript Basics"] = 4;
console.log(student);

// exercise 3

const student2 = {
    name: "Aili",
    credits: 45,
    courseGrades: [
        {
            name: "Intro to Programming",
            grade: 4
        },
        {
            name: "JavaScript Basics",
            grade: 3,
        },
        {
            name: "Functional Programming",
            grade: 5
        }
    ]
}
console.log(`${student2.name} got ${student2.courseGrades[0].grade} from ${student2.courseGrades[0].name}`);

const addCourse = (courseName, courseGrade) => {
    student2.courseGrades.push({ name: courseName, grade: courseGrade });
}

addCourse("rat", 9);
console.log(student2);

// exercise 4

const kompoooter = {
    name: "Buutti SuperCalculator 6000",
    cache: "96 GB",
    clockSpeed: 9001.0,
    overclock: function () { this.clockSpeed += 500 },
    savepower: function () {
        if (this.clockSpeed > 2000) {
            this.clockSpeed = 2000;
        } else { this.clockSpeed /= 2 }
    }
}
kompoooter.overclock();
console.log(kompoooter);
kompoooter.savepower();
console.log(kompoooter);
kompoooter.savepower();
console.log(kompoooter);

// exercise 5 - 8

class Shape {
    constructor(width, height){
        this.width = width;
        this.height = height;
    }
}
class Rectangle extends Shape {
    constructor(width, height) {
        super(width, height);
    }
    getArea() {
        return this.width * this.height;
    }
}

class Ellipse extends Shape {
    constructor(width, height){
        super(width, height);
    }
    getArea() {
        return Math.PI * (this.width/2) * (this.height/2);
    }
}

class Triangle extends Shape {
    constructor(width, height) {
        super(width, height);
    }
    getArea() {
        return (this.width * this.height)/2;
    }
}
class Square extends Rectangle {
    constructor(width){
        super(width);
        this.height = this.width;
    }
    getArea(){
        return this.width**2;
    }
}
class Circle extends Ellipse {
    constructor(width){
        super(width);
        this.height = this.width;
    }
    getArea() {
        return Math.PI * (this.width/2)**2;
    }
}
const rectangle1 = new Rectangle(4, 5);
const ellipse = new Ellipse(7, 5);
const triangle = new Triangle(5, 5);
const square = new Square(5);
const circle = new Circle(3);

console.log(rectangle1);
console.log(ellipse);
console.log(triangle);
console.log(square);
console.log(circle);

rectangle1.area = rectangle1.getArea();
console.log(rectangle1);
ellipse.area = ellipse.getArea();
console.log(ellipse);
triangle.area = triangle.getArea();
console.log(triangle);
square.area = square.getArea();
console.log(square);
circle.area = circle.getArea();
console.log(circle);

// exercise 9

class Robot {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    handleMessage(message) {
        if (message === "moveNorth") { this.y++; }
        else if (message == "moveSouth") { this.y--; }
        else if (message === "moveEast") { this.x++; }
        else if (message == "moveWest") { this.x--; }
    }
}

class FlexibleRobot extends Robot {
    constructor(x, y){
        super(x, y);
    }
    handleMessage(message) {
        if (message === "moveNE"){ this.y++; this.x++;}
        else if (message === "moveNW"){ this.y++; this.x--;}
        else if (message === "moveSE"){ this.y--; this.x++;}
        else if (message === "moveSW"){ this.y++; this.x--;}
        else { super.handleMessage(message)}
        }
}

const robot = new FlexibleRobot(0, 0);
robot.handleMessage("moveNE");
robot.handleMessage("moveNE");
robot.handleMessage("moveEast");
robot.handleMessage("moveEast");
console.log(robot);
