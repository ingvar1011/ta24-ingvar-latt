interface fruits {
    banana: number,
    apple: number,
    mango: number,
    lemon: number
};

const fruits: fruits = {
    banana: 118,
    apple: 85,
    mango: 200,
    lemon: 65
}

const printWeight = (fruit: string) => {
    if (fruit in fruits) {
        console.log(fruit + " weighs " + fruits[fruit] + "grams");
        // AAAAAAAARRRRRRRRRGGGGGGGGGGHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
    } else {
        console.log("There is no such fruit, only fruits are: " +
            Object.keys(fruits).join(", "));
    }
}

printWeight("apple");
printWeight("rat");
