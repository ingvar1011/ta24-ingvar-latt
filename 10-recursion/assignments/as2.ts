const wordArray: string[] = ["The", "quick", "silver", "wolf"];

const sentencify = (words: Array<string>, i: number): string => {

    if (i >= words.length - 1) {
        return words[words.length - 1] + "!";
    }

    return words[i] + " " + sentencify(words, i + 1);
}

console.log(sentencify(wordArray, 0)); // prints "The quick silver wolf!"
console.log(sentencify(wordArray, 1)); // prints "quick silver wolf!"
