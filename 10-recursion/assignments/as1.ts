{
    const F = (number: number): number => {

        if (number === 0) {
            return 0;
        }
        if (number === 1) {
            return 1;
        }
        return (F(number - 2) * 3) + F(number - 1);
    }

    console.log(F(0));
    console.log(F(1));
    console.log(F(2));
    console.log(F(3));
    console.log(F(4));
    console.log(F(5));
    console.log(F(17));
}