// exercise 1

const factorial = (number: number): number => {
    if (number === 0) {
        return 1;
    }
    return number * factorial(number - 1);
}

console.log(factorial(5));

// exercise 2

const stackOverFlow = (number: number): number => {
    console.log(number);
    return stackOverFlow(number + 1);
}

// stackOverFlow(1); //stops at around 11140

// exercise 3

const F = (number: number): number => {

    if (number === 0) {
        return 0;
    }
    if (number === 1) {
        return 1;
    }
    return F(number - 1) + F(number - 2);
}
console.log(F(21));
