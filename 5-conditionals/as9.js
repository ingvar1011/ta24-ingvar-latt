let pc1 = {
    time: 42,
    energy: 600
}
let pc2 = {
    time: 57,
    energy: 480
}

let pc1Eff = pc1.time * pc1.energy;
let pc2Eff = pc2.time * pc2.energy;

let lessEnergy = pc1Eff > pc2Eff;

let moreEfficient = lessEnergy ? "Computer #2" : "Computer #1"

console.log(moreEfficient);