import express from "express";

// Exercise 2

const server = express();

server.listen(3000, () => {
    console.log('Listening to port 3000');
});

server.get("/", (request, response) => {
    response.send("Hello world!");
});

server.get('/endpoint2', (req, res) => {
    res.send('OK');
});

server.get("/:name/:surname", (request, response) => {
    console.log("Params:");
    console.log(request.params);
    console.log("Query:");
    console.log(request.query);
 
    response.send("Hello " + request.params.name);
 });
 