import "dotenv/config";
import jwt from 'jsonwebtoken'

const logger = (req, res, next) => {
    const time = new Date();
    console.log(time);
    console.log(req.method);
    console.log(req.originalUrl);
    if (req.body) {
        console.log(req.body)
    }
    next();
}

const authenticate = (req, res, next) => {
    const auth = req.get('Authorization');
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('1 Invalid token')
    }
    const token = auth.substring(7);
    const secret = process.env.JWT_SECRET;
    try {
        const decodedToken = jwt.verify(token, secret);
        req.user = decodedToken;
        console.log(decodedToken);
        next();
    } catch (error) {
        return res.status(401).send('2 Invalid token')
    }
}

const adminAuthenticate = (req, res, next) => {
    const auth = req.get('Authorization');
    if (!auth?.startsWith('Bearer ')) {
        return res.status(401).send('1 Invalid token')
    }
    const token = auth.substring(7);
    const secret = process.env.ADMIN_SECRET;
    try {
        const decodedToken = jwt.verify(token, secret);
        req.user = decodedToken;
        console.log(decodedToken);
        next();
    } catch (error) {
        return res.status(401).send('2 Invalid token')
    }
}

const unknownEndPoint = (_req, res) => {
    res.status(404).send("Error 404, nothin' here m8");
}

const errorHandler = (error, req, res, next) => {
    console.error("An error occurred:", error);
    res.status(500).send("Internal Server Error");
};


export default { logger, unknownEndPoint, errorHandler, authenticate, adminAuthenticate }