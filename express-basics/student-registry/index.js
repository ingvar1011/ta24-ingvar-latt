import express from "express";
import middleware from "./middleware.js";
import studentRouter from "./studentRouter.js";
import userRouter from "./userRouter.js";
import "dotenv/config";

const PORT = process.env.PORT;

const app = express();

app.use(userRouter);
app.use(studentRouter);
app.use(middleware.logger);
app.use(middleware.unknownEndPoint);
app.use(middleware.errorHandler);

app.listen(PORT);
