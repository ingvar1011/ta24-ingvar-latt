import express from "express";
import argon2 from "argon2";
import "dotenv/config";
import jwt from 'jsonwebtoken'

const adminUsername = process.env.ADMINUSERNAME;
const adminPasswordHash = process.env.ADMINPASSWORDHASH;

let users = [];
const userRouter = express.Router();
userRouter.use(express.json());
userRouter.use(express.urlencoded({ extended: false }));

userRouter.post("/login", async (req, res) => {

    const { username, password } = req.body;
    const user = users.find(user => user.username === username);

    if (!user) {
        return res.status(401).send("Unauthorized");
    }

    try {
        const passwordMatchesHash = await argon2.verify(user.hash, password);
        if (passwordMatchesHash) {
            const token = jwt.sign({ username: user.username }, process.env.JWT_SECRET, { expiresIn: '1h' });

            res.status(200).send(token);
        } else {
            res.status(401).send("Unauthorized");
        }
    } catch (error) {
        console.error("Error verifying password:", error);
        res.status(500).send("Internal Server Error");
    }
});

userRouter.post("/register", async (req, res) => {
    const { username, password } = req.body;
    const hash = await argon2.hash(password);
    users.push({ username, hash });
    console.log(users[users.length - 1]);
    res.status(201).send("Created");
});

userRouter.post("/admin", async (req, res) => {
    const { username, password } = req.body;

    if (username !== adminUsername) {
        return res.status(401).send("Unauthorized");
    }

    try {
        const passwordMatchesHash = await argon2.verify(adminPasswordHash, password);
        if (passwordMatchesHash) {
            console.log("ADMIN");
            const token = jwt.sign({ username: process.env.ADMINUSERNAME }, process.env.ADMIN_SECRET, { expiresIn: '1h' });
            res.status(200).send(token);
        } else {
            res.status(401).send("Unauthorized");
        }
    } catch (error) {
        console.error("Error verifying password:", error);
        res.status(500).send("Internal Server Error");
    }
});

export default userRouter;