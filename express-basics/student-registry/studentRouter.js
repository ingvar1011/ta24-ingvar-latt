import express from 'express';
import middleware from './middleware.js';
import "dotenv/config";

const router = express.Router();
router.use(express.static('public'))
router.use(express.json());
router.use(express.urlencoded({ extended: false }));

router.use(middleware.logger)

const students = [{
    "id": 1,
    "name": "John Doe",
    "email": "john@example.com"
},
{
    "id": 2,
    "name": "Joh De",
    "email": "joh@example.com"
}, {
    "id": 3,
    "name": "Jon Do",
    "email": "jon@example.com"
}];

router.get('/route', (req, res) => {
    res.send('OK')
});

router.get("/students", middleware.authenticate, (req, res) => {
    const studentIds = students.map(student => student.id);
    res.send(studentIds);
});

router.get("/studentsadmin", middleware.adminAuthenticate, (req, res) => {
    res.send(students);
});

router.post("/student", middleware.adminAuthenticate, (req, res) => {
    const { id, name, email } = req.body;

    if (!id || !name || !email) {
        return res.status(400).json({ error: "Missing required parameters" });
    }

    students.push({ id, name, email });
    res.status(201).send("Studdent added successfully");
});

router.put("/student/:id", middleware.adminAuthenticate, (req, res) => {
    const { name, email } = req.body;
    const ID = parseInt(req.params.id);
    const student = students.find(student => student.id === ID);

    if (!student) {
        return res.status(404).json({ error: "Student not found" });
    }

    if (!name && !email) {
        return res.status(400).json({ error: "Missing required parameters" });
    }

    if (name) {
        student.name = name;
    }

    if (email) {
        student.email = email;
    }

    res.status(204).json({ message: "" });
});

router.delete("/student/:id", middleware.adminAuthenticate, (req, res) => {
    const ID = parseInt(req.params.id);
    const indexToDelete = students.findIndex(student => student.id === ID);

    if (indexToDelete === -1) {
        return res.status(404).json({ error: "Student not found" });
    }

    students.splice(indexToDelete, 1);

    res.status(204).json({ message: "" });
});

export default router;