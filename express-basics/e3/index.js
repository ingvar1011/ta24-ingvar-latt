import express from "express";

const app = express();

const users = {};

app.get("/counter/:name", (request, response) => {
    const { name } = request.params;

    if (!users[name]) {
        users[name] = { visitCount: 0 };
    }

    users[name].visitCount++;

    console.log(users);
    console.log(`User: ${name}, Visits: ${users[name].visitCount}`);

    response.send(`Hello ${name}, You have visited ${users[name].visitCount} times.`);
});

app.listen(5000);