import request from 'supertest'
import server from './server.js'

describe('Server', () => {
    it('Returns 404 on invalid address', async () => {
        const response = await request(server)
            .get('/invalidaddress')
        expect(response.statusCode).toBe(404)
    })
    it('Returns 200 on valid address', async () => {
        const response = await request(server)
            .get('/')
        expect(response.statusCode).toBe(200)
        expect(response.text).toEqual('Hello World!')
    })
})
