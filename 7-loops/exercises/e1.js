// Exercise 1
console.log("Exercise 1")

let number = 0;
let n = 1;
while (number < 5) {
    number++;
    n *= number;
}
console.log(n);

// Exercise 2
console.log("Exercise 2")

let num = 0;
while (num < 15) {
    num += 3;
    console.log(num);
}

// Exercise 3
console.log("Exercise 3")

// num = 0;
do {
    num += 3;
    console.log(num);
} while (num < 15);

// Exercise 4
console.log("Exercise 4");
// 1st sequence
console.log("Seq. 1");

for (let i = 0; i <= 1000; i += 100) {
    console.log(i);
}
// 2nd sequence
console.log("Seq. 2");

for (let i = 1; i <= 128; i *= 2) {
    console.log(i);
}
// 3rd sequence
console.log("Seq. 3");

for (let i = 3; i <= 15; i += 3) {
    console.log(i);
}
// 4th sequence
console.log("Seq. 4");

for (let i = 9; i >= 0; i -= 1) {
    console.log(i);
}
// 5th sequence
console.log("Seq. 5");

for (let i = 1; i <= 4; i++) {
    for (let k = 1; k <= 3; k++) {
        console.log(i);
    }
}
// 6th sequence
console.log("Seq. 6");

for (let i = 1; i <= 3; i++) {
    for (let k = 0; k <= 4; k++) {
        console.log(k);
    }
}

// Exercise 5
console.log("Exercise 5");
// e5 with for loop
console.log("for -loop");

for (i = 1; i <= 100; i++) {
    if (i % 3 == 0 && i % 5 == 0) {
        console.log("FizzBuzz");
    } else if (i % 3 == 0) {
        console.log("Fizz");
    } else if (i % 5 == 0) {
        console.log("Buzz");
    }
    else console.log(i);
}
// e5 with while loop
console.log("while -loop");

let j = 1;
while (j <= 100) {
    if (j % 3 == 0 && j % 5 == 0) {
        console.log("FizzBuzz");
    } else if (j % 3 == 0) {
        console.log("Fizz");
    } else if (j % 5 == 0) {
        console.log("Buzz");
    }
    else console.log(j);
    j++;
}

// Exercise 6
console.log("Exercise 6");

let num0 = 1;
for (i = 1; i <= 10; i++) {
    if (i % 3 === 0) {
        continue;
    }
    num0 *= i;
}
console.log(num0);

// Exercise 7
console.log("Exercise 7");

let num01 = 1
for (i = 1; i > 0; i++) {
    num01 *= i;
    if (num01 % 600 === 0) {
        console.log(i);
        break;
    }
}