
const exponentValueList = (n, p) => {
    let num = 1;
    if (p === undefined) {
        p = 2;
    }
    if (n <= 0) {
        console.log("n needs to be positive");
        return;
    } else {
        for (i = 1; i <= n; i++) {
            num *= p;
            console.log(num);
        }
    }
}
exponentValueList(3, 5);