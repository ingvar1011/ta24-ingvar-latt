const allVowels = ["a", "e", "i", "o", "u", "y"];
const countOf = {};

let k = 0;
let totalCount = 0;

function checkSentenceVowels(sentence) {
    for (let k = 0; k < allVowels.length; k++) {
        countOf[allVowels[k]] = 0;
        helper(sentence, k);
        console.log(`${allVowels[k].toUpperCase()} letter count: ${countOf[allVowels[k]]}`)
    }
    console.log("Total vowel count: " + totalCount);
}

const helper = (sentence, k) => {
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i).toLowerCase() === allVowels[k]) {
            countOf[allVowels[k]]++;
            totalCount++;
        }
    }
}

checkSentenceVowels("A wizard's job is to vex chumps quickly in fog.");