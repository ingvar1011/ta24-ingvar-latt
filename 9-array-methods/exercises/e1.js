// exercise 1

let str = "According to all known laws of aviation, there is no way a bee should be able to fly.";
const arr = str.split(" ");
for (let i = 0; i < arr.length; i++) {
    if (arr[i].toLowerCase().startsWith("a")) {
        console.log(arr[i]);
    }
}

// exercise 2

const arr5 = [5, 13, 2, 10, 8];
let product = 1;
arr5.forEach(num => product *= num);
console.log(product);

let avg = 0;
arr5.forEach(num => avg += num / arr5.length);
console.log(avg);
console.log(arr5.length);

// exercise 3

const animals = ["horse", "cow", "dog", "hamster", "donkey", "cat", "parrot"];
// a)
for (let aminal of animals) {
    if (aminal.includes("e")) {
        console.log(aminal);
    }
}
// b)
animals.forEach(animal => {
    if (animal.includes("e")) {
        console.log(animal);
    }
})

// exercise 4

const endsWithT = animals.find(animal => animal.endsWith("t"));
const asd = animals.find(animal => animal.startsWith("d") && animal.endsWith("y"));
console.log(endsWithT);
console.log(asd);

// exercise 5

const firstOverFive = animals.findIndex(animal => animal.length > 5);
console.log(firstOverFive);
console.log(animals[firstOverFive]);

// exercise 6

console.log(animals.filter(animal => animal.includes("o")));
console.log(animals.filter(animal => !animal.includes("h") && !animal.includes("o")));

// exercise 7

animals.filter(animal => animal.includes("o")).forEach(animal => console.log(animal));

// exercise 8

const animalsLength = animals.map(animal => animal.length);
const animalsO = animals.map(animal => animal.includes("o"));

console.log(animals);
console.log(animalsLength);
console.log(animalsO);

// exercise 9

const nums = [4, 7, 9, 2, 30, 12, 5, NaN];

let sum = 0;

nums.forEach(num => {
    if (num === NaN) {
        nums.splice(indexof(NaN), 1, 0);
    } else { sum += num };
});
console.log(sum);

const initialValue = 0;
const sum2 = nums.reduce((accumulator, current) => accumulator + current, initialValue);

console.log(sum2);

// exercise 10

const users = [{ firstName: 'Bradley', lastName: 'Bouley', role: 'Full Stack Resident' },
{ firstName: 'Chloe', lastName: 'Alnaji', role: 'Full Stack Resident' },
{ firstName: 'Jonathan', lastName: 'Baughn', role: 'Enterprise Instructor' },
{ firstName: 'Michael', lastName: 'Herman', role: 'Lead Instructor' },
{ firstName: 'Robert', lastName: 'Hajek', role: 'Full Stack Resident' },
{ firstName: 'Wes', lastName: 'Reid', role: 'Instructor' },
{ firstName: 'Zach', lastName: 'Klabunde', role: 'Instructor' }];

users.forEach(user => console.log(user.firstName));
users.forEach(user => {
    if (user.role.includes("Full Stack Resident")) {
        console.log(user);
    }
});
users.forEach(user => {
    if (user.role.includes("Instructor")) {
        console.log(user.firstName + " " + user.lastName);
    }
})
