const objectArray = [ { x: 14, y: 21, type: "tree", toDelete: false },
                      { x: 1, y: 30, type: "house", toDelete: false },
                      { x: 22, y: 10, type: "tree", toDelete: true },
                      { x: 5, y: 34, type: "rock", toDelete: true },
                      null,
                      { x: 19, y: 40, type: "tree", toDelete: false },
                      { x: 35, y: 35, type: "house", toDelete: false },
                      { x: 19, y: 40, type: "tree", toDelete: true },
                      { x: 24, y: 31, type: "rock", toDelete: false } ];

const objectArray2 = [...objectArray];

objectArray.forEach((object, i) => { 
    if(object === null){}
    else if(object.toDelete){
        objectArray[i] = null;

    }
});

console.log(objectArray);

// b)

const test = objectArray2.map(object => {
    if(object === null || !object.toDelete){
        return object;
    }
    return null;
});

console.log(test);

// c) Modifying array is probably faster/takes less memory than creating a new array.
