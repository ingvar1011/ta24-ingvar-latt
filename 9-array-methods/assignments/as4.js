// a)

const incrementAll = (array) => {
    const incrementedArray = [];
    array.forEach(number => incrementedArray.push(number + 1));
    return incrementedArray;
};
const decrementAll = (array) => {
    const decrementedArray = [];
    array.forEach(number => decrementedArray.push(number - 1));
    return decrementedArray;
};

const numbers = [4, 7, 1, 8, 5];
const newNumbers = incrementAll(numbers);
console.log(newNumbers); // prints [ 5, 8, 2, 9, 6 ]

// b)

const numbersB = [4, 7, 1, 8, 5];
const newNumbersB = decrementAll(numbers);
console.log(newNumbersB); // prints [ 3, 6, 0, 7, 4 ]

