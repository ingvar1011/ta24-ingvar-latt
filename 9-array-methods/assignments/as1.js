
const numbers = [8, 12, 17, 9, 16, 24, 16, 25, 35, 27, 38, 50];

// a)

for (var i = 0; i < numbers.length; i++) {
    if (numbers[i] > 20) {
        console.log(numbers[i]);
        break;
    }
}

// b)

console.log(numbers.find(num => num > 20));

// c)

console.log(numbers.findIndex(num => num > 20));

// d)

numbers.splice(numbers.findIndex(num => num > 20) + 1);

console.log(numbers);