const students = [
    { name: "Sami", score: 24.75 },
    { name: "Heidi", score: 20.25 },
    { name: "Jyrki", score: 27.5 },
    { name: "Helinä", score: 26.0 },
    { name: "Maria", score: 17.0 },
    { name: "Yrjö", score: 14.5 }];

const getGrades = (students) => {
    let grades = [];
    students.forEach(student => {
        if (student.score < 14) { grades.push(0) }
        else if (student.score <= 17) { grades.push(1) }
        else if (student.score <= 20) { grades.push(2) }
        else if (student.score <= 23) { grades.push(3) }
        else if (student.score <= 26) { grades.push(4) }
        else { grades.push(5) }
    });
    // console.log(grades);
    return grades;
}

const studentGrades = getGrades(students);
console.log(studentGrades);