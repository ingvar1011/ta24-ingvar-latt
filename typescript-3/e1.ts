import fs from "fs";

const countDown = (arg: number, func:() => void) => {
    setTimeout(() => {
        func();
    }, arg);
}
countDown(1000, () => console.log("terve"));

const countDown2 = (arg: number, func:(luckyNumber: number) => void) => {
    setTimeout(() => {
        func(7);
    }, arg);
};
countDown2(2000, (numb) => console.log(`the lucky number is ${numb}`));

const matti = {
    name: "matti",
    band: "matti&teppo",
    levyt: ["parhaat 1", "lauluja sinulle", "parhaat 2"],
 }
fs.writeFileSync("matti.json", JSON.stringify(matti), "utf8");

const new_matti_string = fs.readFileSync("matti.json", "utf8");
const new_matti = JSON.parse(new_matti_string);
console.log(new_matti);


// "Exercise 6"

const library = [ 
    {
        author: 'David Wallace',
        title:  'Infinite Jest',
        readingStatus: false,
        id: 1,
    },
    {
        author: 'Douglas Hofstadter',
        title:  'Gödel, Escher, Bach',
        readingStatus: true,
        id: 2,
    },
    {
        author: 'Harper Lee',
        title:  'To Kill A Mockingbird', 
        readingStatus: false,
        id: 3,
    },
 ];

function getBook(id: number) {
    const book = library.find(book => book.id === id);
    return book?.title;
}
function printBookData(id: number) {
    return library.find(book => book.id === id);
}
function printReadingStatus(author: string, title: string) {
    const book = library.find(book => book.author === author || book.title === title);
    return book?.readingStatus;
}
function addNewBook(author: string, title: string) {
    const newBook = {
        author: author,
        title: title,
        readingStatus: false,
        id: (library.length + 1)
    }
    library.push(newBook);
}
function readBook(id: number) {
    library[id-1].readingStatus = true;
    console.log(library);
}
function saveToJSON() {
    fs.writeFileSync("library.json", JSON.stringify(library), "utf8");
}
function loadFromJSON() {
    setTimeout(() => {
        const newLibraryString = fs.readFileSync("library.json", "utf8");
        const newLibrary = JSON.parse(newLibraryString);
        console.log(newLibrary);
    }, 0);
    
}

console.log(getBook(3));
console.log(printBookData(2));
console.log(printReadingStatus("", "Infinite Jest"));
addNewBook("Albert", "Einstein");
readBook(4);
saveToJSON();
loadFromJSON();

// "Exercise 7"

const array: Array<number> = [12, 3, 71, 92, 8, 1, 0];
const array2: Array<string> = ["a", "b", "c", "d"]

const isNumberArray = (arr: Array<unknown>): arr is Array<number> => {

    for(let num of arr){
        if(typeof num !== "number"){
            return false;
        }
    }
    return true;
}

console.log("array 1 is number is: "+ isNumberArray(array));
console.log("array 2 is number is: "+ isNumberArray(array2));