'use strict'

const changeUserText = (username) => {
    userText.textContent = `Welcome, ${username}!`
}
submitUsername.addEventListener('click', () => {
    const userNameInput = document.getElementById('usernameInput')
    const username = userNameInput.value;
    changeUserText(username);
})

window.onload = changeUserText("[username]");