changeColor2.addEventListener("click", () => {
    const randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
    example.style.color = randomColor;
});
const changeColor = () => {
    // const example = document.getElementById('example');
    const randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
    example.style.color = randomColor;
    helloButton.style.color = randomColor;
};
const helloButton = document.getElementById('helloButton');
let counter = 0;
helloButton.addEventListener("click", () => {
    counter++;
    window.alert(`Clicked ${counter} time(s)`);
});