
addEventListener("mouseover", (event) => {
    if (event.target.id === "top" ||
        event.target.id === "left" ||
        event.target.id === "right") {
        event.target.style.background = "yellow";
    }
});

addEventListener("mouseout", (event) => {
    event.target.style.background = "";
});
