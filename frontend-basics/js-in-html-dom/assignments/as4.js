let postId = 0;
const forumPosts = [];
const submitPostButton = document.getElementById("submitPostBtn")

class Post {
    constructor(posterName, post, id) {
        this.posterName = posterName;
        this.post = post;
        this.id = id;
    }
}

submitPostButton.addEventListener("click", () => {

    const posterName = document.getElementById("nameInput").value;
    const post = document.getElementById("postInput").value;

    if (posterName === "" || post === "") {
        return alert("Please don't leave post or name field empty")
    }

    newPost = new Post(posterName, post, postId);
    forumPosts.push(newPost);

    newListing = document.createElement('div');
    newListing.setAttribute("class", "border border-dark-subtle border-3 m-1 rounded-3 row");
    newListing.setAttribute("id", postId);
    newListing.innerHTML = `
    <div class="col-9" id="${postId}">
    <h3 class="mt-3"><u>${posterName}</u></h3>
    </div>
    <div class="col-3 d-flex justify-content-end align-items-center">
    <button type="button" class="mt-3 btn btn-danger btn-sm" onclick="deletePost(${postId})">Delete</button>
    </div>
    <div class="col-12">
    <p class="my-3">${post}</p>
    </div>`;

    document.getElementById("forumPosts").appendChild(newListing);

    document.getElementById("nameInput").value = "";
    document.getElementById("postInput").value = "";

    postId++;
});

const deletePost = (id) => {
    const postToDelete = document.getElementById(id);
    const posts = postToDelete.parentElement;
    posts.removeChild(postToDelete);
}