const students = [
    { name: "Sami", score: 24.75 },
    { name: "Heidi", score: 20.25 },
    { name: "Jyrki", score: 27.5 },
    { name: "Helinä", score: 26.0 },
    { name: "Maria", score: 17.0 },
    { name: "Yrjö", score: 14.5 }
];
let toggleStatus = false;

const toggleButton = document.getElementById("toggleStudentsBtn");

toggleButton.addEventListener("click", () => {

    if (toggleStatus === false) {
        toggleStatus = true;
        const tableHeaders = `
        <table id="studentTable"
        <tr>
        <th>Student</th>
        <th>Score</th>
        </tr>
        </table>`;
        document.getElementById("studentTableContainer").innerHTML = tableHeaders;

        for (student of students) {
            const currentStudent = document.createElement('tr');
            currentStudent.innerHTML = `
            <td>${student.name}</td>
            <td>${student.score}</td>`;
            document.getElementById("studentTable").appendChild(currentStudent);
        }
    }
    else {
        toggleStatus = false;
        const deleteTable = document.getElementById("studentTable");
        const item = deleteTable.parentElement;
        item.removeChild(deleteTable);
    }
});
