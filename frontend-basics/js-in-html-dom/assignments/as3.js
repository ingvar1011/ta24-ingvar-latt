let number = 0;
let minutes = 0;
const seconds = document.getElementById("seconds");

const secondsDrawer = () => {
    if (minutes > 0) {
        seconds.innerHTML = `${minutes} minutes, ${number} seconds`
    } else seconds.innerHTML = ` ${number} seconds`;
}

const counter = () => {

    setInterval(() => {

        secondsDrawer();
        number++;
        if (number === 60) {
            number = 0;
            minutes++;
        }
    }, 1000);

}
counter();