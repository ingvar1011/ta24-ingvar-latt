let itemList = [];
let postId = 0;
const getItems = async () => {
    try {
        itemList = await axios.get('https://fakestoreapi.com/products')
        const items = await itemList.data;

        await drawItems(items);
    } catch (error) {
        console.log('Error fetching data: ', error)
    }
}

const drawItems = async (items) => {
    for (item of items) {
        const newItem = document.createElement('li');

        newItem.classList.add('d-flex', 'justify-content-between', 'align-items-center', 'border');
        newItem.setAttribute("id", postId);

        newItem.innerHTML = `${item.title} $${item.price}
        <button type="button" class="m-1 btn btn-danger btn-sm align-items-end" onclick="deletePost(${postId})">Delete</button>`
        document.getElementById("itemList").appendChild(newItem);
        postId++;
    }
}
const deletePost = async (id) => {
    axios.delete(`https://fakestoreapi.com/products/${id + 1}`)
        .then(response => {
            const postToDelete = document.getElementById(id);
            const posts = postToDelete.parentElement;
            posts.removeChild(postToDelete);
        }).catch(error => {
            console.error('Error deleting resource:', error);
        });
}