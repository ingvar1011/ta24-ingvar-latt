const bookList = [];

const addBookBtn = document.getElementById("addBookBtn");

class Book {
    constructor(bookName, pageCount) {
        this.name = bookName;
        this.pageCount = pageCount;
    };
};

addBookBtn.addEventListener("click", () => {

    const bookName = document.getElementById("bookName").value;
    const pageCount = document.getElementById("pageCount").value;

    const newBook = new Book(bookName, pageCount);
    bookList.push(newBook);

    const bookListItem = document.createElement('li');
    bookListItem.setAttribute("class", "list-group-item");
    bookListItem.innerHTML = `${bookList[(bookList.length - 1)].name} (${bookList[(bookList.length - 1)].pageCount} pages)`

    document.getElementById("bookList").appendChild(bookListItem);

});