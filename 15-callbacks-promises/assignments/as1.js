// a)

const sum = (limit) => {
    let value = 0;
    for(let i = 0; i <= limit; i++){
        value += i;
    }
    return value;
}
// console.log(sum(10));

// b), c)

new Promise((res, rej) => {
    setTimeout(() => res(sum(50000)), 1000);
})
.then((value) => console.log(value));

// d)

const creadeDelayedCalculation = (limit, milliseconds) => {
    return new Promise((res, rej) => {
        setTimeout(() => res(sum(limit)), milliseconds);
    })
}
creadeDelayedCalculation(20000000, 2000).then(value => console.log(value));
creadeDelayedCalculation(50000, 500).then(value => console.log(value));

// e) Because the first one is set to return after 2sec, while the 2nd one after .5sec