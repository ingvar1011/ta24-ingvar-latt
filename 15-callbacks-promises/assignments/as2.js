// a)

const waitFor = async (milliseconds) => {
    // let response;
    await new Promise((resolve) => {
        setTimeout(() => {
            // response = "heh";
            resolve();
        }, milliseconds);
    });
    // console.log(response);
}

// b)

const countSeconds = async () => {
    for(let i = 0; i <= 10; i++){
        await waitFor(1000);
        console.log(i);
    }
}

countSeconds();