// exercise 1
const exercise1 = () => {

    let anotherFunction = function (arg, func) {
        setTimeout(() => {
            func();
        }, 1000);

    }
    anotherFunction(0, () => {
        console.log("3");
        anotherFunction(1000, () => {
            console.log("..2");
            anotherFunction(2000, () => {
                console.log("....1");
                anotherFunction(3000, () => {
                    console.log("GO!")
                });
            });
        });
    });
}
// exercise 2
const exercise2 = () => {

    new Promise((resolve, reject) => {
        const it_works = true;
        if (it_works) {
            resolve();
        } else {
            reject("rejected");
        }
    })
        .then(() => {
            console.log("3");
        })
        .then(() => {
            setTimeout(() => {
                console.log("..2");
            }, 1000)
        }).then(() => {
            setTimeout(() => {
                console.log("....1");
            }, 2000)
        }).then(() => {
            setTimeout(() => {
                console.log("GO!");
            }, 3000)
        })
        .catch((val) => {
            console.log(val);
        });
}
// exercise 3
const exercise3 = () => {

    const getValue = function () {
        return new Promise((res, rej) => {
            setTimeout(() => {
                res({ value: Math.random() });
            }, Math.random() * 1500);
        });
    }

    Promise.all([getValue(), getValue()])
        .then((values) => {
            console.log(`Value 1 is ${values[0].value} and value 2 is ${values[1].value}`);
        });

    const getData = async () => {
        const valueOne = await getValue();
        const valueTwo = await getValue();
        console.log(`Value 1 is ${valueOne.value} and value 2 is ${valueTwo.value}`);
    };

    getData();
}
// exercise1();
// exercise2();
exercise3();