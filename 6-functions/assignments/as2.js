const minimum = (num1, num2, num3) => {
    if (num1 < num2 && num1 < num3) {
        return num1;
    } else if (num2 < num3) {
        return num2;
    } else return num3;
}

let smallest = minimum(187547533, 1254332, 9123);
console.log(smallest);