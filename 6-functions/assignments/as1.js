const hello = (language) => {
    switch (language) {
        case "en":
            console.log("Hello World!");
            break;
        case "ee":
            console.log("Tere maailm!");
            break;
        case "fi":
            console.log("Terve maailma!");
            break;
    }
}
// hello();
hello("ee");
hello("fi");
hello("en");