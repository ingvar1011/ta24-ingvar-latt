const firstTriangle = { width: 7.0, length: 9.5 };
const secondTriangle = { width: 4.3, length: 6.4 };
const thirdTriangle = { width: 6.5, length: 5.0 };

const areaCalc = (triangle) => {
    return (triangle.width * triangle.length) / 2;
}

let firstTriangleArea = areaCalc(firstTriangle);
let secondTriangleArea = areaCalc(secondTriangle);
let thirdTriangleArea = areaCalc(thirdTriangle);

console.log("Area of first triangle: " + firstTriangleArea);
console.log("Area of second triangle: " + secondTriangleArea);
console.log("Area of third triangle: " + thirdTriangleArea);

const biggestArea = (num1, num2, num3) => {
    if (num1 > num2 && num1 > num3) {
        console.log("First triangle has biggest area");
    } else if (num2 > num3) {
        console.log("Second triangle has biggest");
    } else console.log("Third triangle has biggest area!");
}

biggestArea(firstTriangleArea, secondTriangleArea, thirdTriangleArea);