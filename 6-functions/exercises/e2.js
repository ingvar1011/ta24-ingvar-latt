// Exercise 2
console.log("Exercise 2___________")
function multiply(number1, number2) {
    return number1 * number2;
}

const multiply2 = function (number1, number2) {
    return number1 * number2;
}

const multiply3 = (number1, number2) => {
    return number1 * number2;
}

let a = multiply(3, 7);
console.log(a);
let b = multiply2(4, 7);
console.log(b);
let c = multiply3(5, 7);
console.log(c);
let d = multiply3(25, 25);
console.log(d);

// Exercise 3
console.log("Exercise 3___________")

const f = 9;
console.log(f);

const increment = (number) => {
    number++;
    console.log(number);
    return number;
}
increment(f);
console.log(f);

// Exercise 4.1
console.log("Exercise 4.1___________")

const arr = [12, 32, 41, 51];
console.log(arr);

const array1 = (array) => {
    array[0] = "book";
    return array;
}

array1(arr);
console.log(arr);

// Exercise 4.2
console.log("Exercise 4.2___________")

fish = {
    species: "goldfish"
}
console.log(fish);

/**
 * 
 * @param {object} object that gets a boolean value of leFishe
 * @returns the object with leFishe property
 */
const lefishe = (fishe) => {
    fishe.leFishe = false;
    return fishe;
}

lefishe(fish);
console.log(fish);

// Exercise 5
console.log("Exercise 5__________");

const tree = { x: 6, y: 7, hitpoints: 30 };
const rock = { x: 3, y: 11, hitpoints: 90 };
const damage = 15;

const DamageTree = (hit) => {
    tree.hitpoints = tree.hitpoints - hit;
}
const DamageRock = (hit) => {
    rock.hitpoints = rock.hitpoints - hit;
}
const Damage = (target, amnt) => {
    target.hitpoints = target.hitpoints - amnt;
}
DamageTree(7);
Damage(rock, 32);
Damage(tree, 30); // this tree dead as hell
console.log("Rock hitpoints left: " + rock.hitpoints);
console.log("Tree hitpoints left: " + tree.hitpoints);
