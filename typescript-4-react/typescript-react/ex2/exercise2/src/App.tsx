import TestComponent from "./comp.tsx"

function App() {
	const name: string = "Ingvar L2tt"
	const yrOfBirth: number = 1999
	return (
		<>
			<TestComponent name={name} yearOfBirth={yrOfBirth} />
			<TestComponent yearOfBirth={yrOfBirth} />
			<TestComponent name={name} />
			<TestComponent />
		</>
	)
}

export default App
