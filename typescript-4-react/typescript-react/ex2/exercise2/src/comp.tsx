interface PropInterface {
    name?: string,
    yearOfBirth?: number
}
const TestComponent = (props: PropInterface): JSX.Element => {
    const crntYear: number = new Date().getFullYear()

    if (!props.name && !props.yearOfBirth) return <p>??????????????</p>
    if (!props.yearOfBirth) return <p>I'm {props.name}</p>
    else if (!props.name) return <p>I'm {crntYear - props.yearOfBirth} years old</p>
    else return (
        <div>
            <p>I'm {props.name}. I am {crntYear - props.yearOfBirth} years old.</p>
        </div>
    );
}

export default TestComponent;