import { useState } from 'react'
import './App.css'
import TodoNote from './TodoNote'
import { v4 as uuid } from 'uuid'
import InputForm from './InputForm'

function App() {

	interface Todo {
		id: string,
		text: string,
		complete: boolean,
		editMode: boolean
	}
	const [defaultTodos, setDefaultTodos] = useState<Array<Todo>>([
		{ id: uuid(), text: 'Buy potatoes', complete: false, editMode: false },
		{ id: uuid(), text: 'Make food', complete: false, editMode: false },
		{ id: uuid(), text: 'Exercise', complete: false, editMode: false },
		{ id: uuid(), text: 'Do the dishes', complete: false, editMode: false },
		{ id: uuid(), text: 'Floss the teeth', complete: false, editMode: false },
		{ id: uuid(), text: 'Play videogames', complete: true, editMode: false },
	]);


	const toggleCompletion = (id: string) => {
		const newTodos = defaultTodos.map(todo => {
			if (todo.id === id) {
				return {
					...todo,
					complete: !todo.complete
				};
			}
			return todo;
		});
		setDefaultTodos(newTodos);
	};

	const editTodo = (id: string, newText: string) => {
		const newTodos = defaultTodos.map(todo => {
			if (todo.id === id) {
				return {
					...todo,
					text: newText
				};
			}
			return todo;
		});
		setDefaultTodos(newTodos);
	};

	const removeTodo = (id: string) => {
		const newTodos = defaultTodos.filter(todo => {
			if (todo.id !== id) {
				return todo
			}
		});
		setDefaultTodos(newTodos);
	};

	const addTodo = (text: string) => {
		const id: string = uuid();
		const todoText: string = text;
		const newTodos = ([...defaultTodos,
		{
			id: id,
			text: todoText,
			complete: false,
			editMode: false
		}]);
		setDefaultTodos(newTodos);
	}

	// const [searchText, setSearchText] = useState('');
	// const filteredTodos = defaultTodos.filter(todo =>
	// 	todo.text.toLowerCase().includes(searchText.toLowerCase())
	// );

	return (
		<>
			{defaultTodos.map(({ id, text, complete, editMode }) => (
				<TodoNote
					key={id}
					id={id}
					text={text}
					complete={complete}
					editMode={editMode}
					onDeleteClick={() => removeTodo(id)}
					onCheckboxClick={() => toggleCompletion(id)}
					onEditTodoClick={(id: string, newText: string) => editTodo(id, newText)}
				/>
			))}
			<InputForm
				onAddTodo={(text: string) => addTodo(text)}
			/>
			{/* <div>
				<input value={searchText} onChange={e => setSearchText(e.target.value)} />
				{filteredTodos.map(todo => <todo text={todo.text} />)}
			</div> */}
		</>
	)
}

export default App
