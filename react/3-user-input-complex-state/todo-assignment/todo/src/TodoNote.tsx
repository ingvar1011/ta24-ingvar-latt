import { useState } from "react"

interface PropsType {
    id: string,
    text: string,
    complete: boolean,
    editMode: boolean,
    onCheckboxClick: (id:string) => void,
    onEditTodoClick: (id:string, newText:string) => void,
    onDeleteClick: (id:string) => void
}

const TodoNote = (props:PropsType) => {

    const checkboxClick = props.onCheckboxClick;
    const id = props.id;
    const text = props.text;
    const complete = props.complete;
    const [editMode, setEditMode] = useState<boolean>(props.editMode)  
    const [newText, setNewText] = useState<string>(text)

    const onInputTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewText(event.target.value);
      }

    return (
        <>
        <div className="TodoNote"
        style={{
            backgroundColor: complete ? "green" : "red"
        }}>
            <input className="checkbox" 
            type="checkbox" 
            checked={complete} 
            onChange={() => checkboxClick(id)}
            />
            <p>{text}</p>
            {editMode ? (
                <>
                    <input type="text" value={newText} onChange={onInputTextChange}/>
                    <button onClick={() => {
                        setEditMode(!editMode);
                        props.onEditTodoClick(id, newText);
                    }}>Save</button>
                </>
            ) : (
                <button onClick={() => setEditMode(!editMode)}>Edit</button>
            )}
            <div>
            <button className="x-button" onClick={()=>props.onDeleteClick(id)}>X</button>
            </div>
        </div>
        </>
    )
}

export default TodoNote