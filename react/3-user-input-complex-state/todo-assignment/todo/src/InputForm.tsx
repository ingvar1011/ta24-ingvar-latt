import { useState } from "react"

interface PropsType {
    onAddTodo: (text:string) => void
}

const InputForm = (props:PropsType) => {
    const [newTodo, setNewTodo] = useState<string>("");

    const onInputTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTodo(event.target.value);
      }
    const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        props.onAddTodo(newTodo);
    }

    return (
        <>
        <form onSubmit={handleSubmit}>
            <input type="text" 
            placeholder="Add a new note"
            value={newTodo}
            onChange={onInputTextChange}
            />
            <button type="submit">Submit</button>
        </form>
        </>
    )
}

export default InputForm