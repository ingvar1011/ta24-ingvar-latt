import { useState } from "react"

const ObjectState = () => {
    interface StateType {
        counter: number,
        input: string
    }

    const [state, setState] = useState<StateType>({
        counter: 0,
        input: ""
    })
    const [text, setText] = useState<string>('');

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setText(event.target.value);
    }

    const clickHandler = () => {
        const newState: StateType = {
            counter: state.counter + 1,
            input: text
        };
        setState(newState);
        setText("");
    }

    return (
        <>
            <input
                type="text"
                id="textInput"
                value={text}
                onChange={handleChange}
            />
            <button onClick={clickHandler}>{state.counter}</button>
            <p>Saved input text is {state.input}</p>
        </>
    )
}

export default ObjectState