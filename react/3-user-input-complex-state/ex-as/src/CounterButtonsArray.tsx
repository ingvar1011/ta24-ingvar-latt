import { useState } from "react"

const CounterButtonsArray = () => {
    interface CounterType {
        id: number,
        value: number
    }

    const [buttonValue, setButtonValue] = useState<Array<CounterType>>([
        { id: 1, value: 0 },
        { id: 2, value: 0 },
        { id: 3, value: 0 },
        { id: 4, value: 0 }
    ]);

    const clickHandler = (index: number) => {
        const newButtonValue: Array<CounterType> = [...buttonValue];
        newButtonValue[index].value += 1;
        newButtonValue[3].value += 1;
        setButtonValue(newButtonValue);
    };

    const buttons = buttonValue.map((button, index) => {
        if (index === buttonValue.length - 1) {
            return <p key={index}>{button.value}</p>;
        }
        return (
            <button key={index} onClick={() => clickHandler(index)}>
                {button.value}
            </button>
        );
    });

    return (
        <>
            {buttons}
        </>
    )
}

export default CounterButtonsArray