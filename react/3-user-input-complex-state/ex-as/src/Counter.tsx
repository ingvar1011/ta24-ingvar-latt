interface PropsType {
    id: string,
    counter: number,
    minimumCounter: number,
    onCounterClick: (id: string) => void,
    onDeleteClick: (id: string) => void
}

const Counter = (props: PropsType) => {
    if (props.counter >= props.minimumCounter)
        return (
            <>
                <div>
                    <button key={props.id}
                        onClick={() => props.onCounterClick(props.id)}>
                        {props.counter}
                    </button>
                    <button onClick={() => props.onDeleteClick(props.id)}>Delete</button>
                </div >
            </>
        )
}

export default Counter