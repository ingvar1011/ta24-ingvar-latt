import { useState } from 'react'
import './App.css'
import ObjectState from './ObjectState'
import CounterButtonsArray from './CounterButtonsArray'
import Counter from './Counter'
import { v4 as uuidv4 } from 'uuid';

interface CounterType {
  id: string,
  counter: number
}

function App() {

  const [name, setName] = useState('Pasi')

  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  }

  const [counters, setCounters] = useState<Array<CounterType>>([
    { id: uuidv4(), counter: 0 },
    { id: uuidv4(), counter: 0 },
    { id: uuidv4(), counter: 0 },
  ]);

  const [minimumCounter, setMinimumCounter] = useState<number>(0);

  const [showInput, setShowInput] = useState<boolean>(true);

  const incrementCounter = (id: string) => {
    setCounters(prevCounters => {
      return prevCounters.map(counter => {
        if (counter.id === id) {
          return { ...counter, counter: counter.counter + 1 };
        }
        return counter;
      });
    });
  };
  const addNewCounter = () => {
    setCounters([...counters, { id: uuidv4(), counter: 0 }])
  };
  const deleteCounter = (id: string) => {
    setCounters(counters.filter(counter => counter.id !== id));
  };
  const minimumHandler = (e) => {
    setMinimumCounter(e.target.value);
  };

  return (
    <div className='App'>
      <p>{name}</p>
      <legend>Name:</legend>
      <input value={name}
        onChange={onNameChange} />

      <Form />
      <Form2 />
      <ObjectState />
      <CounterButtonsArray />
      <div>
        {counters.map(({ id, counter }) => (
          <Counter
            key={id}
            id={id}
            minimumCounter={minimumCounter}
            counter={counter}
            onCounterClick={() => incrementCounter(id)}
            onDeleteClick={() => deleteCounter(id)} />
        ))}
      </div>
      <button onClick={() => addNewCounter()}>Add a new counter!</button>
      <div>

        <label htmlFor="minCounter">Minimum counter </label>
        <input
          type='number'
          value={minimumCounter}
          onChange={minimumHandler}
          id='minCounter' />

        <label htmlFor="checkbox">Toggle minimum input</label>
        <input
          type="checkbox"
          name="Show inputs"
          id="checkbox"
          checked={showInput}
          onChange={() => setShowInput(!showInput)} />
      </div>
    </div>
  )
}

const Form = () => {
  const [name, setName] = useState<string>('')
  const [yourString, setYourString] = useState<string>('')

  const [checked, setChecked] = useState<boolean>(false);
  const handleChange = () => setChecked(!checked);

  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  }

  const onBtnClick = () => {
    setYourString(name);
    setName('')
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('You clicked submit.');
  }

  return (
    <div className='App'>
      <h1>Your string is: {yourString}</h1>
      <input value={name}
        onChange={onNameChange} />
      <button onClick={onBtnClick} >Submit</button>
      <div>
        <label>
          <input
            type="checkbox"
            checked={checked}
            onChange={handleChange}
          />
          My Value
        </label>
        <p>Is "My Value" checked? {checked.toString()}</p>
      </div>
      <div>
        <form onSubmit={handleSubmit}>
          <button type="submit">Submit</button>
        </form>
      </div>
    </div>
  )
}

const Form2 = () => {

  const [name, setName] = useState<string>('')
  const [yourString, setYourString] = useState<string>('')

  const onNameChange = (event) => {
    setName(event.target.value);
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setYourString(name);
    setName('')
    console.log('You clicked submit.');
  }

  return (
    <>
      <form onSubmit={handleSubmit}>
        <h1>Your string is: {yourString}</h1>
        <input value={name}
          onChange={onNameChange} />

        <button type="submit">Submit</button>
      </form>
    </>
  )
}

export default App