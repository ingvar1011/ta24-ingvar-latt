import { useEffect, useState } from "react";
import noteService from "./noteService";
import React from "react";

interface Note {
	id?: string | number;
	content: string;
	date: string;
	important: boolean;
}

const Notes = () => {
	const [notes, setNotes] = useState<Array<Note>>([]);
	const [newNote, setNewNote] = useState<string>("");

	const newNoteHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
		setNewNote(event.target.value);
	};

	useEffect(() => {
		noteService
			.getAll()
			.then(initialNotes => {
				setNotes(initialNotes);
			})
			.catch(error => {
				console.error('Error fetching data:', error);
			});
	}, []);

	const toggleImportanceOf = (id: string | number) => {
		const note = notes.find(n => n.id === id)
		if (note) {
			const changedNote: Note = { ...note, important: !note.important }
			noteService
				.update(id, changedNote)
				.then(returnedNote => {
					setNotes(notes.map(
						note => note.id !== id ? note : returnedNote
					))
				});
		}
	}

	const addNote = (event: React.MouseEvent<HTMLButtonElement>) => {
		event.preventDefault()
		const noteObject: Note = {
			content: newNote,
			date: new Date().toISOString(),
			important: Math.random() > 0.5
		};

		noteService
			.create(noteObject)
			.then(returnedNote => {
				setNotes(notes.concat(returnedNote))
				setNewNote('');
			})
	}

	return (
		<>
			<h1>Notes</h1>

			<ul>
				{notes.map(note =>
					<li>
						{note.content}
						<button onClick={() => {
							if (note.id) {
								toggleImportanceOf(note.id);
							}
						}}>
							Toggle!
						</button>
					</li>
				)}
			</ul>

			<form action="">
				<input type="text"
					value={newNote}
					onChange={newNoteHandler}
				/>
				<button onClick={addNote}>
					Add note
				</button>
			</form>
		</>
	)
}

export default Notes