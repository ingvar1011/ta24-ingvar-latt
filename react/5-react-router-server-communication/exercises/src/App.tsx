import './App.css'
import Home from './Home'
import About from './About'
import Links from './Links'
import {
  BrowserRouter as Router,
  Routes, Route, Link
} from "react-router-dom";
import Notes from './Notes'




function App() {

  const padding = { padding: 5 }

  return (
    <>
      <Router>
        <div>
          <Link style={padding} to="/">home</Link>
          <Link style={padding} to="/about">about</Link>
          <Link style={padding} to="/links">links</Link>
        </div>

        <Routes>
          <Route path="/about" element={<About />} />
          <Route path="/links" element={<Links />} />
          <Route path="/" element={<Home />} />
        </Routes>

        <ul>
          <Notes />
        </ul>

      </Router>


    </>
  )
}

export default App
