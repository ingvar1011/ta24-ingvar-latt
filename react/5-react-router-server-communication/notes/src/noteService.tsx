import axios from 'axios'
const baseUrl = 'http://localhost:3000/notes';


interface Todo {
    id?: string,
    text: string,
    complete: boolean,
    editMode: boolean
}

const getAll = async () => {
    const request = axios.get(baseUrl);
    const response = await request;
    return response.data;
}

const create = async (newObject: Todo) => {
    const request = axios.post(baseUrl, newObject);
    const response = await request;
    return response.data;
}

const update = async (id: string | number, newObject: Todo) => {
    const request = axios.put(`${baseUrl}/${id}`, newObject);
    const response = await request;
    return response.data;
}

const removeTodo = async (id: string) => {
    const request = axios.delete(`${baseUrl}/${id}`);
    const response = await request;
    return response.data;
}

export default { getAll, create, update, removeTodo }