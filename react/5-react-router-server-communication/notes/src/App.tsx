import { useState, useEffect } from 'react'
import './App.css'
import TodoNote from './TodoNote'
// import { v4 as uuid } from 'uuid'
import InputForm from './InputForm'
import noteService from './noteService'

function App() {

  interface Todo {
    id?: string,
    text: string,
    complete: boolean,
    editMode: boolean
  }
  const [defaultTodos, setDefaultTodos] = useState<Array<Todo>>([]);

  useEffect(() => {
    noteService
      .getAll()
      .then(initialNotes => {
        setDefaultTodos(initialNotes);
      })
      .catch(error => {
        console.error('Error fetching data:', error);
      });
  }, []);


  const toggleCompletion = (id: string | number) => {
    const Todo = defaultTodos.find(n => n.id === id)
    if (Todo) {
      const changedTodo: Todo = { ...Todo, complete: !Todo.complete }
      noteService
        .update(id, changedTodo)
        .then(returnedTodo => {
          setDefaultTodos(defaultTodos.map(
            todo => todo.id !== id ? todo : returnedTodo
          ))
        });
    }
  }

  const editTodo = (id: string, newText: string) => {
    const Todo = defaultTodos.find(todo => todo.id === id)
    if (Todo) {
      const changedTodo: Todo = { ...Todo, text: newText }
      noteService
        .update(id, changedTodo)
        .then(returnedTodo => {
          setDefaultTodos(defaultTodos.map(
            todo => todo.id !== id ? todo : returnedTodo
          ))
        })
    }
  };

  const removeTodo = (id: string) => {
    const newTodos = defaultTodos.filter(todo => {
      if (todo.id !== id) {
        return todo
      }
    });
    noteService
      .removeTodo(id)
    setDefaultTodos(newTodos);
  };

  const addTodo2 = (text: string) => {
    const todoObject: Todo = {
      text: text,
      complete: false,
      editMode: false
    };
    noteService
      .create(todoObject)
      .then(returnedTodo => {
        setDefaultTodos(defaultTodos.concat(returnedTodo))
      })
  }

  const [searchText, setSearchText] = useState('');

  return (
    <>
      <h1>Todo Notes</h1>
      {defaultTodos.map(({ id, text, complete, editMode }) => {
        if (text.toLowerCase().includes(searchText.toLowerCase())) {
          return (
            <TodoNote
              key={id}
              id={id}
              text={text}
              complete={complete}
              editMode={editMode}
              onDeleteClick={() => removeTodo(id)}
              onCheckboxClick={() => toggleCompletion(id)}
              onEditTodoClick={(id: string, newText: string) => editTodo(id, newText)}
            />
          )
        }
      })}
      <InputForm
        onAddTodo={(text: string) => addTodo2(text)}
      />
      <div>
        <label htmlFor="searchInput">Search </label>
        <input value={searchText} onChange={e => setSearchText(e.target.value)} id='searchInput' />
      </div>
    </>
  )
}

export default App
