import './App.css'
import { Greetings } from "./Greetings.jsx"
import { Planets } from "./Planets.jsx"

function App() {
  const fullName = "Ingvar L2tt"
  const age = 25
  const planetList = [
    { name: "Hoth", climate: "Ice" },
    { name: "Tattooine", climate: "Desert" },
    { name: "Alderaan", climate: "Temperate" },
    { name: "Mustafar", climate: "Volcanic", }
  ];
  
  return (
    <>
      <h1>Assignment 1</h1>
      <Greetings fullname = {fullName} age = {age}/>

      <h1>Assignment 2</h1>
      <div className="container">
      <img className="img" src="https://gitea.buutti.com/education/academy-assignments/raw/branch/master/React/1.%20React%20Basics/r2d2.jpg" alt="" srcSet="" />
      <h2>Hello, I am R2D2!</h2>
      <p><i>BeeYoop  BeeDoopBoom Weeop DEEpaEEya!</i></p>
      </div>
      <h1>Assignment 3</h1>
      <div className="as3">
      <Planets planetList = {planetList}/>
      </div>
    </>
  )
}

export default App
