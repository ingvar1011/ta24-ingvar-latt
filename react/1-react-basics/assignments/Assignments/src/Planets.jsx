/* eslint-disable react/prop-types */

export const Planets = (props) => {
    const planets = props.planetList
    const planetsDOM = planets.map(planet => 
        <tr key={planet}>
            <td key={planet}>{planet.name}</td>
            <td key={planet}>{planet.climate}</td>
        </tr>
)
    return (
        <table>
            <tr>
                <th>Planet</th>
                <th>Climate</th>
            </tr>
            {planetsDOM}
        </table>
    )
}