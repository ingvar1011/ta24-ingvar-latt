export const Greetings = (props) => {
    const fullname = props.fullname
    const age = props.age
    return (
        <p>My name is {fullname}, I am {age} years old</p>
    )
}