/* eslint-disable react/prop-types */
import { Hello } from "./Hello.jsx"
import { Year } from "./Year.jsx"
import { ListNames } from "./ListNames.jsx"


const User = (props) => {
    return (
        <div>
            <p>{props.name}</p>
            <p>{props.birthyear}</p>
            <p>{props.generateUsername(props.name, props.birthyear)}</p>
        </div>
    )
}
const App = () =>{
    const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];
    const generateUsernameSimple = (name, birthyear) => name + birthyear % 100;
    const currentYear = Year()
    const now = new Date();
    const name1 = 'peter'
    return (
    <div>
        <h1>Greetings, it is {now.toString()}</h1>
        <Hello name = 'george' currentYear = {currentYear}/>
        <Hello name = {name1} currentYear = {currentYear}/>
        <Hello />
        <User
            name="Mikko"
            birthyear={1989}
            generateUsername={generateUsernameSimple}
        />
        <ul>
            <ListNames nameList = {namelist}/>
        </ul>
    </div>
  )
}

export default App
