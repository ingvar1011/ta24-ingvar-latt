export const ListNames = (props) => {
    const nameList = props.nameList;
    const nameDOMs = nameList.map((name, index) => 
        <li key={name}>
            <span style={{ fontWeight: index % 2 === 1 ? 'normal' : 'bold', fontStyle: index % 2 === 1 ? 'italic' : 'normal' }}>
                {name}
            </span>
        </li>
    );
    return nameDOMs;
};