/* eslint-disable react/prop-types */
export const Hello = (props) => {
    const name = props.name
    const year = props.currentYear
    return (
        <div>
            <h1>Hello {name}</h1>
            <h2>It is year {year}</h2>
        </div>
    )
}