import DisappearingButton from "./DisappearingButton"

const Buttons = () => {
    return (
        <>
            <DisappearingButton i = {1}/>
            <DisappearingButton i = {2}/>
            <DisappearingButton i = {3}/>
            <DisappearingButton i = {4}/>
            <DisappearingButton i = {5}/>
        </>
    )
}

export default Buttons