import { useState } from "react"

interface PropsInterface {
    names: Array<string>
}

const Bingo = (props: PropsInterface) => {
    const names = props.names;
    const [colorToggles, setColorToggles] = useState<boolean[]>(names.map(() => false));
    const [bingoMessage, setBingoMessage] = useState<string>("")

    const handleButtonClick = (index: number) => {
        // Create a new array with the state of the clicked button toggled
        const newColorToggles = [...colorToggles];
        newColorToggles[index] = !newColorToggles[index];
        setColorToggles(newColorToggles);
        bingoCheck(newColorToggles);
    };

    const bingoCheck = (statusArray: boolean[]) => {
        let bingoCount: number = 0;
        // Horizontal check
        for (let j = 0; j < 25; j += 5) {
            let bingo: number = 0;
            for (let i = j; i < j + 5; i++) {
                if (statusArray[i] === true) {
                    bingo++;
                    if (bingo === 5) {
                        bingoCount++;
                    }
                }
            }
        }
        // Vertical check
        for (let j = 0; j < 5; j++) {
            let bingo: number = 0;
            for (let i = j; i < 25; i += 5) {
                if (statusArray[i] === true) {
                    bingo++;
                    if (bingo === 5) {
                        bingoCount++;
                    }
                }
            }
        }
        // Diagonal check
        for (let j = 0; j < 6; j += 4) {
            let bingo: number = 0;
            if (j === 0) {
                for (let i = j; i < 25; i += 6) {
                    if (statusArray[i] === true) {
                        bingo++;
                        if (bingo === 5) {
                            bingoCount++;
                        }
                    }
                }
            } else {
                for (let i = j; i < 25; i += 4) {
                    if (statusArray[i] === true) {
                        bingo++;
                        if (bingo === 5) {
                            bingoCount++;
                        }
                    }
                }
            }
        }
        
        if (bingoCount === 1) {
            setBingoMessage(`BINGO`);
        } else if (bingoCount > 1) {
            setBingoMessage(`X${bingoCount} BINGO`);
        } else setBingoMessage("");
    }

    const namesDOM = names.map((name, index) => (
        <button
            style={{
                backgroundColor: colorToggles[index] ? "green" : "red",
                color: colorToggles[index] ? "white" : "black"
            }}
            onClick={() => handleButtonClick(index)}
            className={"buttonBackground"}
            key={index}
        >
            {name}
        </button >
    ));
    return (
        <div>
            <div className="grid-container">
                {namesDOM}
            </div>
            <h1 className="bingoMessage" >{bingoMessage}</h1>
        </div>
    )
}

export default Bingo