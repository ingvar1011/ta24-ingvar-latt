import { useState } from 'react'
import './App.css'
import Buttons from './Buttons'
import Bingo from './Bingo';

function App() {
	const names = [
		"Anakin Skywalker",
		"Leia Organa",
		"Han Solo",
		"C-3PO",
		"R2-D2",
		"Darth Vader",
		"Obi-Wan Kenobi",
		"Yoda",
		"Palpatine",
		"Boba Fett",
		"Lando Calrissian",
		"Jabba the Hutt",
		"Mace Windu",
		"Padmé Amidala",
		"Count Dooku",
		"Qui-Gon Jinn",
		"Aayla Secura",
		"Ahsoka Tano",
		"Ki-Adi-Mundi",
		"Luminara Unduli",
		"Plo Koon",
		"Kit Fisto",
		"Shmi Skywalker",
		"Beru Whitesun",
		"Owen Lars"
	];

	const [buttonToggle, setButtonToggle] = useState<boolean>(true)

	return (
		<>
			<h1>Assignment 1</h1>

			<button onClick={() => {
				if (buttonToggle === true) setButtonToggle(false)
				else setButtonToggle(true)
			}}>Toggle Text</button>

			{buttonToggle === true ? (
				<p>Fear is the path to the darkside</p>
			)
				: (<p></p>)}

			<h1>Assignment 2</h1>

			<Buttons />

			<h1>Assignment 3</h1>

			<Bingo names={names} />
		</>
	)
}

export default App
