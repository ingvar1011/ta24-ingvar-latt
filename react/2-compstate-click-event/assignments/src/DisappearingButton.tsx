import { useState } from "react"

interface PropsType {
    i: number
}
const DisappearingButton = (props:PropsType) => {
    const [toggleButton, setToggleButton] = useState<boolean>(true)
    return (
        <>
        {toggleButton === true ? (
        <>
            <button onClick={() => setToggleButton(false)
            } >Button {props.i}</button>
		</>) 
		: (<></>)}
        </>
    )
}

export default DisappearingButton