import Family from "./Family";
import "./App.css";
import React, { useState, MouseEvent } from "react";
import Counter from "./CounterButtons";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

interface Person {
	name: string;
	age: number;
}

function App(): JSX.Element {

	const people: Person[] = [
		{ name: 'Alice', age: 30 },
		{ name: 'Bob', age: 35 },
		{ name: 'Charlie', age: 40 },
		{ name: 'Donna', age: 45 }
	];

	const [countSum, setCountSum] = useState<number>(0)
	const [buttonToggle, setButtonToggle] = useState<boolean>(true)
	const myFunction = (event: MouseEvent<HTMLButtonElement>): void => {
		console.log(event);
	};

	return (
		<>
			<h1>Exercise 1 and 2</h1>
			<div className="app" >
				<Family people={people} />
			</div>
			<button onClick={myFunction}>
				Click me
			</button>
			<h1>Exercise 3</h1>

			<button onClick={() => {
				if (buttonToggle === true) setButtonToggle(false)
				else setButtonToggle(true)
			}}>Toggle Counters</button>

			{buttonToggle === true ? (
				<div>
					<Counter setCountSum={setCountSum} />
					<Counter2 setCountSum={setCountSum} />
				</div>)
				: (<p></p>)}

			<p>{countSum}</p>

			<Mailbox />
		</>
	);
}

interface CounterProps {
	setCountSum: (countSum: number) => void;
}

const Counter2 = (props: CounterProps) => {
	const [count, setCount] = useState(0);

	return (
		<>
			<button onClick={() => {
				setCount(count + 1)
				props.setCountSum((prevCountSum) => prevCountSum + 1);
			}}>
				{count}
			</button>
		</>
	);
};

function Mailbox() {
	const [
		unreadMessages,
		setUnreadMessages
	] = useState<number>(1);
	return (
		<div>
			<h1>Hello!</h1>
			{unreadMessages > 0 &&
				<h2>
					You have {unreadMessages}
					unread messages.
				</h2>
			}
		</div>
	);
}


export default App;
