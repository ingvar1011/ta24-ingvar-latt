import "./Person.css"
interface PropsInterface {
    name: string,
    age: number
}
const Person = (props: PropsInterface): JSX.Element => {
    return (
        <p className="person" >{props.name} is {props.age} years old!</p>
    )
}

export default Person