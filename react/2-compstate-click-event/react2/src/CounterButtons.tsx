import { useState } from "react";

interface CounterProps {
    setCountSum: (countSum: number) => void;
}

const Counter = (props: CounterProps) => {
    const [count, setCount] = useState(0);
    const [count2, setCount2] = useState(0);
    

    return (
    <>
        <button onClick={() => {
            setCount(count + 1)
            props.setCountSum((prevCountSum) => prevCountSum + 1);
            }}>
            {count}
        </button>
        
        <button onClick={() => {
            props.setCountSum((prevCountSum) => prevCountSum + 1);
            setCount2(count2 + 1)
        }}>
            {count2}
        </button>
        <div>total counter: {count + count2}</div>
    </>
    );
};


export default Counter