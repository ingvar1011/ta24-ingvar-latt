import Person from "./Person"
import "./Family.css"
interface PersonArray {
    name: string;
    age: number;
}

const Family = ({ people }: { people: PersonArray[] }): JSX.Element => {
    return (
        <>
            <h3 className="family" >Family Members</h3>
            {people.map((person, index) => (
                <Person key={index} name={person.name} age={person.age} />
            ))}
        </>
    )
}

export default Family