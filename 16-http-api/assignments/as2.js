import axios from "axios";
import fetch from "node-fetch";

const getFakeStoreProducts = async () => {
    const products = await axios.get('https://fakestoreapi.com/products');
    const productsData = products.data;
    const productNames = productsData.map(product => product.title);
    console.log(productNames);
    // console.log(products.data);
}

const addFakeStoreProduct = (name, price, description, category) => {

    axios
        .post('https://fakestoreapi.com/products', {
            title: name,
            price: price,
            description: description,
            category: category
        })
        .then((response) => console.log(response.data))
        .catch((error) => console.log(error));

}



const deleteFakeStoreProduct = (id) => {
    if (!id) {
        console.log("Please enter id to delete");
        return;
    }
    axios.delete(`https://fakestoreapi.com/products/${id}`)
        .then((response) => {
            const deletedProduct = response.data;
            const deletedName = deletedProduct.title;
            console.log(`DELETED: ${deletedName}`);
        })
        .catch((error) => {
            console.error("An error occurred:", error);
        });
}


getFakeStoreProducts();
addFakeStoreProduct('Tomato', 5.29, "le very nice tomatooo :Ddd", 'food');
deleteFakeStoreProduct(5);