import axios from "axios";

const getUniversities = async () => {

const response = await axios.get(`http://universities.hipolabs.com/search?country=Finland`);

return response.data;

}
const nameArray = await getUniversities().then((response) => {
    return response.map(university => university.name);
});

console.log(nameArray);