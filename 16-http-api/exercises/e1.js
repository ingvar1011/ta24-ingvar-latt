// const axios = require("axios");
import axios from "axios";

// exercise 1 (and 3)

const exercise1 = () => {

    const getPostById = async (id) => {
        try {
            const response = await axios.get(id);
            return response.data;
        } catch (error) {
            console.error('Error:', error);
        }
    }
    
    const displayPostAndUser = async () => {
        try {
            const post = await getPostById("https://jsonplaceholder.typicode.com/posts/1");
            const user = await getPostById(`https://jsonplaceholder.typicode.com/users/${post.userId}`);
            console.log(post);
            const postId = post.id;
            const postTitle = post.title;
            const username = user.username;
            console.log(`Post #${postId} by ${username}: "${postTitle}".`);
            
            // exercise 3
            axios
            .patch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
                title: 'New title'
            })
            .then((response) =>  console.log(response.data))
            .catch((error) => console.log(error));

        } catch (error) {
            console.error('Error:', error);
        }
    }
    
    displayPostAndUser();
    

}
exercise1();

const exercise2 = () => {

    const getPostById = async (id) => {
        try {
            const response = await axios.get(id);
            return response.data;
        } catch (error) {
            console.error('Error:', error);
        }
    }

const getPostComments = async (id) => {
    try {
    const postComments = await getPostById(`https://jsonplaceholder.typicode.com/comments?postId=${id}`);
    console.log(postComments);
    } catch (error) {
        console.log('Error: ', error);
    }
}

getPostComments(2);

}
// exercise2();

const exercise3 = () => {

    axios
    .post('https://jsonplaceholder.typicode.com/posts', {
        userId: 213,
        id: 101,
        title: 'Hei',
        body: 'Hopsti!'
    })
    .then((response) =>  console.log(response.data))
    .catch((error) => console.log(error));

}

// exercise3();

