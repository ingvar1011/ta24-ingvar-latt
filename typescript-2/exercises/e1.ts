const func = (str: string) => {
    let strProperties = {
        length: 0,
        words: 0
    };
    strProperties.length = str.length;
    strProperties.words = str.split(" ").length;
    return strProperties;
}

const sentenceProperties = func("asd gdsa fsd fsdfsa");
console.log(sentenceProperties);

// Exercise 3 Arrays

const grades: Array<string | number> = [1, "2", 3, "4", 5];

grades.forEach(grade => {
    if (grade > "3") console.log(grade);
});

// Exercise 4 Arrays

const randNums: number[] = [];
for (let i = 0; i < 10; i++) {
    randNums.push(Math.floor(Math.random() * 100));
}
console.log(randNums);

randNums.sort();
console.log(randNums);

randNums.reverse();
console.log(randNums);

// Exercise 5 Objects

interface shipProperties {
    hullBreached: boolean,
    fillLevel: number,
    sunk?: boolean;
}
let ship: shipProperties;

ship = {
    hullBreached: true,
    fillLevel: 0
}

const isItSinking = (ship: shipProperties) => {
    if (ship.hullBreached) {
        for (let i = 0; ship.fillLevel <= 100; i++) {
            ship.fillLevel += 20;
            console.log(ship.fillLevel + "%");

            if (ship.fillLevel >= 100) {
                ship.sunk = true;
                return;
            }
        }
    }
}

isItSinking(ship);
console.log(ship);