const arr = ["the", "quick", "brown", "fox"];

console.log(arr);

console.log(arr[1] + " " + arr[2]);

arr[2] = "gray";

console.log(arr[2]);
arr.push("over", "lazy", "dog");
console.log(arr);
arr.unshift("pangram:");
console.log(arr);

arr.splice(5, 0, "jump");
arr.splice(7, 0, "the");
console.log(arr);

arr.shift();
arr.splice(4, 5);
arr.splice(2, 1);
console.log(arr);

const array6 = ["banana", "apple", "pear", "pineapple", "lemon"];

for (let i = 0; i < array6.length; i++) {
    if (array6[i].length > 5) {
        console.log(array6[i]);
    };
};

const arr7 = [4, 7, 11, 5, 6, 9, 15, 7];
for (let i = 0; i < arr7.length; i++) {
    if (arr7[i] < 7) {
        arr7.splice(i, 1);
        i--;
    }
}
console.log(arr7);
console.log("Exercise 8");
const arr8 = [5, 7, 2, 9, 3, 13, 15, 6, 17, 24];

for (const num in arr8) {
    if (arr8[num] % 3 === 0) {
        console.log(arr8[num]);
    }
}
console.log("------")
for (const num of arr8) {
    if (num % 3 === 0) {
        console.log(num);
    }
}

const arra = ['cherry', 'banana', 'coconut', 'apple', 'pear',
    'pineapple', 'lemon', 'pumpkin'];
const array = [];

for (const fruit of arra) {
    if (fruit.length > 6) {
        array.push(fruit);
    }
}
console.log(array);