const array = [4, 19, 7, 1, 9, 22, 6, 13];

const findLargest = (array) => {
    let big = 0;
    for (let i = 0; i < array.length; i++) {
        if (array[i] > big) {
            big = array[i];
        }
    } return big;
}

const largest = findLargest(array);
console.log(largest);