const isPalindrome = (word) => {
    let test = "";
    word = word.toLowerCase();

    for (let i = word.length - 1; i >= 0; i--) {
        test += word.charAt(i);
    }
    if (word === test) {
        return true;
    } else return false;
}
let value = isPalindrome("saippuakivikauppias");
console.log(value);