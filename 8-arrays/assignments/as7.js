const array = [-4, 19, 7, 1, -9, 22, 6, -13];

const sortNumberArray = (arr) => {

    for (i = 0; i < arr.length; i++) {
        let largestNum = Math.min(...arr);
        for (let crntNum = i; crntNum < arr.length; crntNum++) {
            if (arr[crntNum] > largestNum) {
                largestNum = arr[crntNum];
            }
        }
        arr.splice(arr.indexOf(largestNum), 1);
        arr.unshift(largestNum);
    }
}

sortNumberArray(array);
console.log(array);