const sentence = "this is a short sentence";

const reverseWords2 = (sentence) => {
    let a = 0;
    let arr = [""];
    // const words = {};
    for (let i = 0; i < sentence.length; i++) {
        if (sentence.charAt(i) === " ") {
            a++;
            arr[a] = "";
        } if (arr[a] === undefined) {
            arr[a] = sentence.charAt(i);
        }
        arr[a] = arr[a] + sentence.charAt(i);
        if(arr[a].charAt(0) !== " "){
            arr[a] = " " + arr[a];
        }

    }
    let reversedWord = "";
    let arr2 = [];
    let b = 0;
    for (i = 0; i < arr.length; i++) {
        a = 0;
        if(arr[i] === undefined){
            break;
        }
        for (j = 0; j < arr[i].length; j++) {
            if (arr2[i] === undefined) {
                arr2[i] = "";
            }
            b = arr[i].length - 1 - j;
            arr2[i] = arr2[i] + arr[i].charAt(b);
        }
        reversedWord = reversedWord + arr2[i];
    }
    return reversedWord;
}
const reversed = reverseWords2(sentence);
console.log(reversed); // prints "siht si a trohs ecnetnes"