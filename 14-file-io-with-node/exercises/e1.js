import fs from "fs";
// const fs = require("fs");

// exercise 1

const text = fs.readFileSync("./textFile.txt", 'utf-8');
let textArray = text.split(" ");
let newText = "";

for (let i = 0; i < textArray.length; i++) {
    if (textArray[i].toLowerCase().includes("joulu")) {
        textArray[i] = "kinkku";
    } else if (textArray[i].toLowerCase().includes("lapsilla")) {
        textArray[i] = "poroilla"
    }
    newText += textArray[i] + " ";
}

const writeFile = fs.createWriteStream("./newText.txt");
writeFile.write(newText, (err) => {
    if (err) console.log(err);
    else console.log("success");
});

// exercise 2

const forecast = {
    day: "monday",
    temperature: 20,
    cloudy: true,
    sunny: false,
    windy: false,
};

const forecastJSON = JSON.stringify(forecast, null, 4);

fs.writeFileSync("./forecast_data.json", forecastJSON, "utf-8");

const readJSON = fs.readFileSync("./forecast_data.json", "utf-8");

const readJSONData = JSON.parse(readJSON);

readJSONData.temperature = 23;

const updatedJSONData = JSON.stringify(readJSONData, null, 4);

fs.writeFileSync("./forecast_data.json", updatedJSONData, "utf-8");