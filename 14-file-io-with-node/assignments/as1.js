const readline = require("readline-sync");
// import readline from "readline-sync";

let promptCounter = 0;
let botName = "dumb";
const welcomeMessage = "Hi! I am a dumb chat bot You can check all the things I can do by typing 'help'.";
const helpMessage = `-----------------------------
Here's a list of commands that I can execute! 
help: Opens this dialog.
hello: I will say hello to you
botInfo: I will introduce myself
botName: I will tell my name
botRename: You can rename me
forecast: I will forecast tomorrows weather 100% accurately
quit: Quits the program.
-----------------------------`;

console.log(welcomeMessage);

while (true) {
    const answer = readline.question("Give me an answer: ");

    if (answer === "help") {
        promptCounter++;
        console.log(helpMessage);
    }
    else if (answer === "hello") {
        promptCounter++;
        const hello = readline.question("What is your name: ");
        console.log(`Hello there ${hello}!`);
    }
    else if (answer === "botInfo") {
        promptCounter++;
        console.log(`I am a dumb bot. You can ask me almost anything :). You have already asked me ${promptCounter} questions.`)
    }
    else if (answer === "botName") {
        promptCounter++;
        console.log(`My name is currently ${botName}. If you want to change it, type botRename.`)
    }
    else if (answer === "botRename") {
        promptCounter++;
        let newName = readline.question("Type my new name please: ")
        const nameAnswer = readline.question(`Are you happy with the name ${newName}, (type yes or no): `)
        if (nameAnswer === "yes") {
            botName = newName;
            console.log(`I was renamed to ${botName}`);
        } 
        else if (nameAnswer === "no") {
            console.log(`Name not changed, my name is ${botName}`);
        }
    }
    else if (answer === "forecast") {
        promptCounter++;

        let temperature = Math.floor(50 * Math.random()) - 20;
        let cloudy = Math.floor(10 * Math.random());
        if (cloudy > 5) {
            cloudy = "yes";
        } else cloudy = "no";
        let sunny = Math.floor(10 * Math.random());
        if (sunny > 5) {
            sunny = "yes";
        } else sunny = "no";
        let windy = Math.floor(10 * Math.random());
        if (windy > 5) {
            windy = "yes";
        } else windy = "no";

        console.log(`
        Tomorrows weather will be...
        Temperature: ${temperature}°C
        Cloudy: ${cloudy}
        Sunny: ${sunny}
        Wind: ${windy}
        `)
    }
    else if (answer === "quit") {
        break;
    }
}
