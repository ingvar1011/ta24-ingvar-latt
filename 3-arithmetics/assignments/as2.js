const speed = 90;
const distance = 359.9;
const hour = 60;
const time = distance / speed;
// console.log("time in hours "+time); 

let timeHours = Math.floor(time);
// console.log("full hours " +timeHours);

const timeMinutes = distance/(speed/hour);
// console.log("Total time in minutes "+timeMinutes);

let remainingMinutes = Math.round(timeMinutes%hour);

if (remainingMinutes == 60) {
    remainingMinutes=0;
    timeHours++;
}
// console.log("Remaining minutes "+remainingMinutes);
console.log(`It takes ${timeHours} hours and ${remainingMinutes} minutes to travel ${distance} km at the speed of ${speed} km/h`);