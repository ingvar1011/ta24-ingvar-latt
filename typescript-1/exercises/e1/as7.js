"use strict";
const fruit1 = {
    fruit: "pear",
    weight: 178
};
const fruit2 = {
    fruit: "lemon",
    weight: 120
};
const fruit3 = {
    fruit: "apple",
    weight: 90
};
const fruit4 = {
    fruit: "mango",
    weight: 150
};
const avgWeight = (fruit1.weight + fruit2.weight + fruit3.weight + fruit4.weight) / 4;
const weightDif1 = Math.abs(avgWeight - fruit1.weight);
const weightDif2 = Math.abs(avgWeight - fruit2.weight);
const weightDif3 = Math.abs(avgWeight - fruit3.weight);
const weightDif4 = Math.abs(avgWeight - fruit4.weight);
if (weightDif1 < weightDif2 && weightDif1 < weightDif3 && weightDif1 < weightDif4) {
    console.log(fruit1.fruit);
}
else if (weightDif2 < weightDif3 && weightDif2 < weightDif4) {
    console.log(fruit2.fruit);
}
else if (weightDif3 < weightDif4) {
    console.log(fruit3.fruit);
}
else
    console.log(fruit4.fruit);
