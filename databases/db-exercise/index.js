import express from "express";
import { createProductsTable } from "./db.js";
import dotenv from "dotenv";
import router from "./router.js"

dotenv.config()

const server = express();

createProductsTable();

server.use(express.json())
server.use('/products', router);

const { PORT } = process.env;
server.listen(PORT, () => {
  console.log("Products API listening to port", PORT);
});
