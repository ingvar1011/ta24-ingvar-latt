import { Router } from "express";
import dao from "./dao.js"

const router = Router();

router.post('/', async (req, res) => {
    const product = req.body;
    const result = await dao.insertProduct(product);
    const storedProduct = { id: result.rows[0], ...product };
    res.send(storedProduct);
});

router.get('/:id', async (req, res) => {
    const result = await dao.readProduct(req.params.id);
    const product = result;
    res.send(product);
});

router.put('/:id', async (req, res) => {
    const product = { id: req.params.id, ...req.body };
    console.log(product);
    if (!req.body.name || !req.body.price) {
        res.send("Missing parameters, please include both name and price, in correct order: first name, then price")
    } else {
        await dao.updateProduct(product);
        res.send(product);
    }
});

router.delete('/:id', async (req, res) => {
    const id = req.params.id;
    await dao.deleteProduct(id);
    res.status(200).send('Deleted');
});

router.get('/', async (_req, res) => {
    const result = await dao.listAll();
    console.log(result.rows);
    res.send(result.rows);
});

export default router;
