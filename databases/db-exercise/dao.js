// import { v4 as uuidv4 } from 'uuid';
import { exec } from 'child_process';
import { executeQuery } from './db.js';
import router from './router.js';
import { query } from 'express';

const insertProduct = async (product) => {
    const params = [product.name, product.price];
    const query = "INSERT INTO products (name, price) VALUES ($1, $2)";
    console.log(`Inserting a new product ${product.name}...`);
    const result = await executeQuery(query, params);
    console.log(`New product ${product.name} inserted successfully.`);
    return result;
};

const readProduct = async (id) => {
    const query = "SELECT * FROM products WHERE id = $1";
    console.log(`Reading product ${id}`)
    const result = await executeQuery(query, [id]);
    console.log(result.rows[0]);
    return result.rows[0];
};

const updateProduct = async (product) => {
    const { id, name, price } = product;

    const query = "UPDATE products SET name = $2, price = $3 WHERE id = $1";
    console.log(`Updating product ${id}`);
    const result = await executeQuery(query, [id, name, price]);

    if (result.rowCount === 0) {
        console.log(`Product with ID ${id} not found.`);
        return null;
    }
    console.log(result.rows[0]);
    return result;
};

const deleteProduct = async (id) => {
    console.log(`Deleting product ${id}`);
    const query = "DELETE FROM products WHERE id = $1"
    const result = await executeQuery(query, [id]);
    return result;
};

const listAll = async () => {
    const query = "SELECT * FROM public.products ORDER BY id ASC"
    const result = executeQuery(query);
    return result;
}
export default { insertProduct, readProduct, updateProduct, deleteProduct, listAll }